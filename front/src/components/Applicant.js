/* Applicant component: card-like profile */
import React, { createRef } from 'react';


/*
  Props:
    - applicant: the applicant document to display
    - storeDraggedObject: callback to let the parent store data on drag
*/
class Applicant extends React.Component {
  constructor(props) {
    super(props)
    this.ref = createRef()
    this.handleDragStart = this.handleDragStart.bind(this)
    this.handleDragEnd = this.handleDragEnd.bind(this)
  }

  handleDragStart(event) {
    const applicant = this.props.applicant;
    console.debug('Drag started', applicant.id)

    // attach the id of the dragged applicant
    event.dataTransfer.setData('text/plain', applicant.id)

    if (typeof this.props.storeDraggedObject !== 'undefined') {
      this.props.storeDraggedObject({
        applicantId: applicant.id,
        height: this.ref.current.offsetHeight
      })
    }

    if (typeof this.props.dragStarted !== 'undefined') {
      this.props.dragStarted()
    }
  }

  handleDragEnd(event) {
    console.debug('Drag ended')
    if (typeof this.props.storeDraggedObject !== 'undefined') {
      this.props.storeDraggedObject(undefined)
    }
    if (typeof this.props.dragEnded !== 'undefined') {
      this.props.dragEnded()
    }
  }

  render() {
    const applicant = this.props.applicant;

    return (
      <div className="applicant" draggable="true" onDragStart={this.handleDragStart} onDragEnd={this.handleDragEnd} ref={this.ref}>
        <div>
          <img src={applicant.avatar} alt={applicant.name} />
          <div>
            <h3>{applicant.name}</h3>
            <h4>{applicant.title}</h4>
          </div>
        </div>
        <footer>&nbsp;</footer>
      </div>
    )
  }
}

export default Applicant;
