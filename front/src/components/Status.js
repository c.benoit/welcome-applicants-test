/* Status component: Trello-like column */
import React, {createRef} from 'react';

import Applicant from './Applicant';

/*
  Props:
    - status: the status document to display
    - onUpdate: callback to notify the parent of data update (status change)
    - draggedObject: the data attached to the dragged element
    - storeDraggedObject: callback to let the parent store data on drag
*/
class Status extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.ref = createRef()
    this.handleDrop = this.handleDrop.bind(this)
    this.handleDragOver = this.handleDragOver.bind(this)
    this.handleDragLeave = this.handleDragLeave.bind(this)
  }

  handleDragOver(event) {
    event.preventDefault()

    const hoverRow = this.calculateTargetRowIndex(event)
    if (hoverRow !== this.state.hoverRow) {
      this.setState({ hoverRow })
    }
  }

  handleDragLeave(event) {
    const currentElt = this.ref.current
    if (
      (event.clientY >= currentElt.offsetTop && event.clientY <= currentElt.offsetTop + currentElt.offsetHeight) &&
      (event.clientX >= currentElt.offsetLeft && event.clientX <= currentElt.offsetLeft + currentElt.offsetWidth)
    ) {
      // do nothing: we are still within the column
    } else {
      // we left the column
      this.setState({hoverRow: undefined})
    }
  }

  calculateTargetRowIndex(event) {
    // calculate the position where the drop is targeted
    const rows = Array.from(this.ref.current.children)
    const rowIndex = rows.findIndex(row => event.clientY >= row.offsetTop && event.clientY <= row.offsetTop + row.offsetHeight)
    return rowIndex >= 0 ? rowIndex : Math.max(0, rows.length - 1)
  }

  handleDrop(event) {
    event.preventDefault()

    console.debug('Drop')

    if (typeof this.props.storeDraggedObject !== 'undefined') {
      this.props.storeDraggedObject(undefined)
    }
    this.setState({hoverRow: undefined})

    // retrieve info from drag event
    const applicantId = event.dataTransfer.getData('text/plain')
    const statusId = this.props.status.id
    const position = this.calculateTargetRowIndex(event)

    this.props.onUpdate(applicantId, statusId, position)
  }

  render() {
    const status = this.props.status
    const placeholder = "placeholder"
    let rows = status.applicants

    if (typeof this.state.hoverRow !== 'undefined') {
      // copy the array
      rows = rows.slice()
      // insert placeholder in place of the dragged applicant
      if (
        this.props.draggedObject &&
        this.state.hoverRow > 0 &&
        this.state.hoverRow - 1 >= 0 &&
        status.applicants[this.state.hoverRow - 1] &&
        status.applicants[this.state.hoverRow - 1].id === this.props.draggedObject.applicantId
        ) {
        rows.splice(this.state.hoverRow - 1, 0, placeholder)
      } else {
        // insert hover placeholder row in its position
        rows.splice(this.state.hoverRow, 0, placeholder)
      }
    }
    let applicantsCount = rows.length
    // if this is the dragged item, decrement total column count
    if (this.props.draggedObject && status.applicants.find(applicant => applicant.id === this.props.draggedObject.applicantId)) {
      applicantsCount -= 1
    }

    return (
      <div className="status">
        <header>
          <h2>{status.name}</h2>
          <span className="badge">{applicantsCount}</span>
        </header>
        <ul onDrop={this.handleDrop} onDragOver={this.handleDragOver} onDragLeave={this.handleDragLeave} ref={this.ref}>
          {rows.map(row => {
            if (row === placeholder) {
              return (
                <li key="targetPlaceholder" className="targetPlaceholder">
                  <div style={{height: (this.props.draggedObject ? this.props.draggedObject.height : 0) + 'px'}} />
                </li>
              )
            } else {
              const applicant = row
              let className = (this.props.draggedObject && applicant.id === this.props.draggedObject.applicantId) ? 'dragged' : ''
              return (
                <li key={applicant.id} className={className}>
                  <Applicant applicant={applicant} storeDraggedObject={this.props.storeDraggedObject} />
                </li>
              )
            }
          })}
        </ul>
      </div>
    )
  }
}

export default Status;
