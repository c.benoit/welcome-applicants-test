import { render, screen } from '@testing-library/react';
import App from './App';

const testDataSet = [
  {
    id: "1",
    name: "À rencontrer",
    applicants: [
      {
        id: "1",
        name: "Steve Jobs",
        title: "Producteur de pommes",
        avatar: "https://clipground.com/images/steve-jobs-clipart-9.jpg"
      },
      {
        id: "2",
        name: "Dave Lauper",
        title: "Expert geek",
        avatar: "https://i.pinimg.com/236x/6f/3e/58/6f3e58b09b8ec34a7ac040393beba52d--mad-magazine-cartoon-characters.jpg"
      }
    ]
  },
  {
    id: "2",
    name: "Entretien",
    applicants: []
  }
]

test('content is here: statuses and applicants', () => {
  render(<App statuses={testDataSet} />);

  expect(screen.getByText(/Entretien/i)).toBeInTheDocument();
  expect(screen.getByText(/rencontrer/i)).toBeInTheDocument();

  expect(screen.getByText(/Steve Jobs/i)).toBeInTheDocument();
  expect(screen.getByText(/Dave Lauper/i)).toBeInTheDocument();
});

test('badge of elements count in columns', () => {
  const container = document.createElement('div')

  render(<App statuses={testDataSet} />, container);

  const statuses = Array.from(document.getElementsByClassName('status'));

  expect(statuses.length).toEqual(testDataSet.length);

  statuses.forEach(statusElt => {
    const badgeValue = statusElt.getElementsByClassName('badge')[0].textContent
    const applicantsCount = Array.from(statusElt.getElementsByClassName('applicant')).length

    expect(badgeValue).toEqual('' + applicantsCount)
  });
});
