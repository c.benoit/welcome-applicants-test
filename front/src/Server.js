/* Manages all communication with the server (API+websocket) */
import {Socket} from 'phoenix';

class Server {
  static serverProtocol = 'http'
  static serverUrl = 'localhost:4000'
  static channelTopic = 'welcome:notifications'
  static websocketUrl = `ws://${this.serverUrl}/socket`

  // Returns a Promise
  static loadApplicants() {
    return this.fetchJson('/api/applicants')
  }

  // Returns a Promise
  static loadStatuses() {
    return this.fetchJson('/api/statuses')
  }

  // Returns a Promise
  static updateApplicantStatus(applicantId, statusId, position) {
    console.debug('Posting applicant update', applicantId, statusId, position)

    return fetch(this.buildHttpUrl(`/api/applicants/${applicantId}`), {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        status: statusId,
        position
      })
    })
  }

  // Returns the channel object
  static connectToChannel() {
    console.debug('Connecting to server via websocket...')
    let socket = new Socket(this.websocketUrl, {params: {}})
    socket.connect()
    console.debug('Connected')
  
    console.debug(`Joining channel ${this.channelTopic}...`)
    let channel = socket.channel(this.channelTopic, {})
    channel.join()
    .receive('ok', _resp => {
      console.debug('Joined successfully')
    })
    .receive('error', resp => {
      console.debug('Unable to join', resp)
    })
    .receive('timeout', () => console.error('Network timeout!'))
  
    return channel
  }

  static buildHttpUrl(route) {
    return `${this.serverProtocol}://${this.serverUrl}${route}`
  }

  // Returns a Promise
  static fetchJson(route) {
    console.debug(`Querying server route ${route}...`)

    return fetch(this.buildHttpUrl(route))
    .then((response) => {
      if (!response.ok) {
        throw new Error(`HTTP error with status ${response.status}`)
      }
      return response.json()
    })
  }
}

export default Server;
