import React from 'react';
import './App.css';

import Status from './components/Status';
import Server from './Server';

const createStatusTree = (applicants, statuses) => {
  console.debug(`Loaded ${applicants.length} applicants with ${statuses.length} statuses from the server`)

  return statuses.map(status => {
    return {
      applicants: status.applicant_ids.map(applicantId => applicants.find(applicant => applicant.id === applicantId)),
      ...status
    }
  })
}

/*
  Props:
    - statuses [optional]: 'status tree' - used for tests
*/
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = { statuses: [] }
    this.handleUpdate = this.handleUpdate.bind(this)
    this.storeDraggedObject = this.storeDraggedObject.bind(this)
  }

  componentDidMount() {
    if (typeof this.props.statuses !== 'undefined') {
      // use the test data if provided
      this.setState({ statuses: this.props.statuses })
    } else {
      // load data from the server
      Promise.all([Server.loadApplicants(), Server.loadStatuses()])
      .then(([applicantData, statusData]) => {
        this.setState({
          statuses: createStatusTree(applicantData.applicants, statusData.statuses),
          applicants: applicantData.applicants
        })
      })
      .catch(err => {
        console.error('Failed to load data from server', err)
      })
      .then(() => this.setupWebsocketNotification())
      .catch(err => {
        console.error('Failed to load data from server', err)
      })
    }
  }

  reloadStatuses() {
    if (typeof this.state.applicants !== 'undefined') {
      Server.loadStatuses()
      .then(statusData => {
        this.setState({ statuses: createStatusTree(this.state.applicants, statusData.statuses) })
      })
    }
  }

  setupWebsocketNotification() {
    let channel = Server.connectToChannel()

    // When an update message is sent, we update all the statuses
    channel.on('update', msg => {
      console.debug('received update notification', msg)
      this.reloadStatuses()
    })
  }

  storeDraggedObject(data) {
    this.setState({draggedObject: data})
  }

  handleUpdate(applicantId, statusId, position) {
    // preemptively update local state to avoid flashing
    let statuses = this.state.statuses.slice()
    const originStateIndex = statuses.findIndex(status => status.applicants.find(applicant => applicant.id === applicantId))

    if (originStateIndex >= 0) {
      // remove applicant from old status
      const originApplicantIndex = statuses[originStateIndex].applicants.findIndex(applicant => applicant.id === applicantId)

      if (originApplicantIndex >= 0) {
        const applicant = statuses[originStateIndex].applicants[originApplicantIndex]

        statuses[originStateIndex].applicants.splice(originApplicantIndex, 1)

        // add it to new status
        const destStateIndex = statuses.findIndex(status => status.id === statusId)
        if (destStateIndex >= 0) {
          statuses[destStateIndex].applicants.splice(position, 0, applicant)
        }
      }
    }
    this.setState({statuses: statuses})

    // now call the server for the real update
    Server.updateApplicantStatus(applicantId, statusId, position)
  }

  render() {
    return (
      <div className="App">
        <ul className="columns">
        {this.state.statuses.map((status) => {
         return (
           <li key={status.id}>
             <Status status={status} onUpdate={this.handleUpdate} draggedObject={this.state.draggedObject} storeDraggedObject={this.storeDraggedObject} />
           </li>
         ) 
        })}
        </ul>
      </div>
    )
  }
}

export default App;
