# Welcome Applicants Test

## Goals

This web app displays job applicants profiles with the ability to change their status via a drag and drop Trello-like experience.

A change of status should automatically be pushed to all opened web sessions.

## Getting started

### Requirements

* Elixir + Mix
* Node + Npm

### Setup

```shell
welcome-applicants-test% cd back
back% mix deps.get
back% cd ../front
front% npm install
```

### Run

Use two shells:

```shell
welcome-applicants-test% cd back
back% iex -S mix phx.server
iex(1)> Fixtures.one_applicant_two_statuses()
```

```shell
welcome-applicants-test% cd front
front% npm start
```

This will open a browser session pointing to http://localhost:3000

> This should be run on a modern desktop browser.

### Run tests

```shell
welcome-applicants-test% cd back
back% mix test
back% cd ../front
front% npm test
```

## Design

### Languages/Frameworks

The backend is in Elixir / Phoenix.

The frontend uses React.

### Project structure

The front is standalone, not part of the Phoenix backend. This way several people can work in parallel on both sides. Though here both front and back projects are inside the same git repository to be able to clone/read the whole thing more easily, in real life there would probably be two separate repositories.

The backend is made up of an umbrella project to better separate the presentation layer (API) from the domain layer. This way that layer could be scrapped, or another presentation layer (command line tool for instance) could be added to the project without needing to change anything.

### Back architecture

[Onion architecture](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/) where the domain model and logic do not depend upon the presentation layer or even the data storage implementation.

Here's the dependency chain:

```
model < logic < presentation layer (API)
              < infrastructure (data store)
```

This is achieved using dependency injection : the store implementation (in-memory state genserver) is defined in the configuration, the domain layer does not depend on it, only on its interface (Dependency Inversion principle).

This might seem a bit extreme for such a small project (almost no business logic at all), though if we assume this is the beginning of a much larger project, this setup will pay off in the end. For instance the data store can be swapped with a database implementation without changing anything to the rest of the code and without breaking any existing unit test.

### Data structure

A simple row of applicants containing each their status would have been a simple solution. But this meant not being able to specify the position of each applicant in its status column.

Another solution would have been to store rows of statuses containing each the applicant document. Having the applicant as a separate table/document is cleaner and requires fewer data to transmit over the wires if only the status changes (what this project is all about).

Statuses and applicants are not hardcoded on the front side, only set as fixtures in the backend to more easily set up the test. Methods are available to add applicants/statuses or attach a status to an applicant.

### Back <-> Front communication

The front fetches and posts data to the back via a simple Rest API (3 routes), with CORS enabled.

A websocket Phoenix channel emits a broadcast when a change requires some reload that is then done via an API route.

Other possible ways could have been:
* All communication via websockets only, though it's not meant to transmit large amounts of data so this solution was discarded
* GraphQL subscriptions, but that sounded like too big a task here

### Dependencies

Minimal dependencies were used: the fewer the better!

#### Back

* [phoenix](https://hex.pm/packages/phoenix) : Elixir web framework
* [cors_plug](https://hex.pm/packages/cors_plug) : handles CORS OPTIONS requests
* [uuid](https://hex.pm/packages/uuid) : generates unique string identifiers
* [mox](https://hex.pm/packages/mox) : mock library for tests

#### Front

* [create-react-app](https://www.npmjs.com/package/create-react-app) : React toolset
* [phoenix](https://www.npmjs.com/package/phoenix) : for websockets

### Back tests

Tests are written with mocks (via the [Mox library](https://github.com/dashbitco/mox)) to ensure elements are unit tested independently.

A high test code coverage is aimed with 100% coverage of written code (`mix test --cover`).

### Front tests

The automated front tests do not cover the API calls, it uses a fixed dataset instead.

The drag and drop features were not tested automatically either, that would have been a huge task that I consider out of scope here. Comprehensive manual tests were done for this part, with multiple data cases in different browsers.

### Discussion

The drag and drop features could have been more easily implemented with something like [react-smooth dnd](https://github.com/kutlugsahin/react-smooth-dnd) or [react-beautiful-dnd](https://www.npmjs.com/package/react-beautiful-dnd), but it felt like cheating not trying to implement this part myself.

There are a few glitches on that side, having something working perfectly would require much more work I guess.

It looks like the drag and drop API (`draggable` property) might not be the best for this after all. The libraries above seem to be tracking cursor position and moving the dragged element with a `position: fixed` attribute.

## Features

### What's included

* Ability to set the drop position in the target column (if more than one item - use `Fixtures.two_applicants_three_statuses()` to try it out)
* The counter of applicants per status matches the expected behaviour
* Logs with different levels
* Unit tests
* Continuous Integration (Gitlab CI)

### What's been left out

The following features have not been included, I feel they are out of scope for this project but should be kept in mind for future developments.

* A data change on the back end (for instance to add new applicants) is not broadcasted to clients
* Data persistence
* Authorisation / user sessions
* Avatar image storage management
* i18n
* Alternate solution other than drag and drop for mobile devices
* Html accessibility / semantics
* Production environments / CD / monitoring ..
