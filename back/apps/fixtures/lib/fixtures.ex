defmodule Fixtures do
  @moduledoc """
  Fixtures to populate the datastore
  """
  alias Core.Model.{Status, Applicant}
  alias Core.DataStore.MemoryStore

  def one_applicant_two_statuses() do
    MemoryStore.clear()

    applicant_id = %Applicant{
      name: "Steve Jobs",
      title: "Producteur de pommes",
      avatar: "https://clipground.com/images/steve-jobs-clipart-9.jpg"
    }
    |> MemoryStore.insert_applicant()

    status_id = %Status{name: "À rencontrer"}
    |> MemoryStore.insert_status()

    %Status{name: "Entretien"}
    |> MemoryStore.insert_status()

    # set the first status
    Core.update_applicant_status(applicant_id, status_id)
  end

  @doc """
  Adds an extra applicant with another status
  """
  def two_applicants_three_statuses() do
    one_applicant_two_statuses()

    applicant_id = %Applicant{
      name: "Bill Gates",
      title: "Poseur de fenêtres",
      avatar: "https://live.worldbank.org/sites/default/files/styles/focal_point_bio_detail/public/experts/billgates.jpg"
    }
    |> MemoryStore.insert_applicant()

    status_id = %Status{name: "Recalés"}
    |> MemoryStore.insert_status()

    Core.update_applicant_status(applicant_id, status_id)
  end
end
