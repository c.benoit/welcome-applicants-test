defmodule FixturesTest do
  use ExUnit.Case
  doctest Fixtures

  test "fixtures changes dataset" do
    Fixtures.one_applicant_two_statuses()

    assert Enum.count(Core.get_applicants()) == 1
    assert Enum.count(Core.get_statuses()) == 2

    Fixtures.two_applicants_three_statuses()
    assert Enum.count(Core.get_applicants()) == 2
    assert Enum.count(Core.get_statuses()) == 3
  end
end
