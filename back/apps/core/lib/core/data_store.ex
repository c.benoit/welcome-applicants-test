defmodule Core.DataStore do
  @moduledoc """
  Interface for a data store
  """

  alias Core.Model.{Applicant, Status, UniqueId}

  @callback insert_applicant(Applicant.t()) :: UniqueId.t()

  @callback insert_status(Status.t()) :: UniqueId.t()

  @callback remove_applicant_status(UniqueId.t()) :: :ok | :error

  @callback set_applicant_status(UniqueId.t(), UniqueId.t(), nil | integer()) :: :ok | :error

  @callback get_statuses() :: list(Status.t())

  @callback get_applicants() :: list(Applicant.t())

end
