defmodule Core.Model.Status do
  @moduledoc """
    Model for the status list of job applicants
  """
  alias Core.Model.UniqueId

  @enforce_keys [:name]
  @derive Jason.Encoder
  defstruct id: nil, name: "", applicant_ids: []

  @typedoc """
    Applicants status type
  """
  @type t :: %__MODULE__{id: UniqueId.t()|nil, name: String.t(), applicant_ids: list(String.t())}
end
