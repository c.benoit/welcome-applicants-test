defmodule Core.Model.Applicant do
  @moduledoc """
    Job Applicant model
  """
  alias Core.Model.UniqueId

  @enforce_keys [:name]
  @derive Jason.Encoder
  defstruct id: nil, name: "", title: "", avatar: ""

  @typedoc """
    An applicant type
  """
  @type t :: %__MODULE__{id: UniqueId.t()|nil, name: String.t(), title: String.t(), avatar: String.t()}
end
