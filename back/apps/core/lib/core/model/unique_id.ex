defmodule Core.Model.UniqueId do
  @moduledoc """
    Interface for a unique string identifier
  """
  @type t :: String.t()

  @doc """
  Helper that be used for in-memory data store
  """
  def generate(), do: UUID.uuid4()

end
