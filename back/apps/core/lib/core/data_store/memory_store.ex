defmodule Core.DataStore.MemoryStore do
  @moduledoc """
    Memory based data store for this project.
    No persistence here, it's a simple GenServer!
    Behaves like a database where Applicants and Statuses would
    live as separate objects (tables or documents), for easier transition
    in the future.
  """
  alias Core.DataStore
  alias Core.Model.{Applicant, Status, UniqueId}
  use GenServer
  require Logger
  # implements the DataStore interface
  @behaviour DataStore

  # the name of the genserver will be our module name to avoid name clashes
  @server_name __MODULE__

  ## CLIENT

  def start_link(args) do
    GenServer.start_link(__MODULE__, args, name: @server_name)
  end

  @doc """
  Synchronous call, returns the new applicant id
  """
  @impl DataStore
  def insert_applicant(%Applicant{} = applicant) do
    GenServer.call(@server_name, {:insert_applicant, applicant})
  end

  @doc """
  Synchronous call, returns the new status id
  """
  @impl DataStore
  def insert_status(%Status{} = status) do
    GenServer.call(@server_name, {:insert_status, status})
  end

  @doc """
  Synchronous call, returns :ok
  """
  @impl DataStore
  def remove_applicant_status(applicant_id) do
    GenServer.call(@server_name, {:remove_applicant_status, applicant_id})
  end

  @doc """
  Synchronous call, returns :ok or :error
  """
  @impl DataStore
  def set_applicant_status(applicant_id, status_id, position \\ nil) do
    GenServer.call(@server_name, {:set_applicant_status, applicant_id, status_id, position})
  end

  @doc """
  Synchronous call, returns a list of statuses
  """
  @impl DataStore
  def get_statuses() do
    GenServer.call(@server_name, {:get_statuses})
  end

  @doc """
  Synchronous call, returns a list of applicants
  """
  @impl DataStore
  def get_applicants() do
    GenServer.call(@server_name, {:get_applicants})
  end

  @doc """
  Synchronous call, clears all data from the storage
  """
  def clear() do
    GenServer.call(@server_name, {:clear})
  end


  ## SERVER

  @impl GenServer
  def init(_args) do
    {:ok, blank_state()}
  end

  defp blank_state() do
    %{
      applicants: [],
      statuses: []
    }
  end

  @impl GenServer
  def handle_call({:clear}, _src, _state), do: {:reply, :ok, blank_state()}

  def handle_call({:get_statuses}, _src, state) do
    {:reply, state.statuses, state}
  end

  def handle_call({:get_applicants}, _src, state) do
    {:reply, state.applicants, state}
  end

  def handle_call({:set_applicant_status, applicant_id, status_id, position}, _src, state) do
    # find the applicant and the status
    with status_index when not is_nil(status_index) <- find_index_of_item_with_id(state.statuses, status_id),
      status <- Enum.at(state.statuses, status_index),
      applicant_index when not is_nil(applicant_index) <- find_index_of_item_with_id(state.applicants, applicant_id) do

      index = position
      |> check_position(Enum.count(status.applicant_ids))

      # add the new applicant to the status
      status = %{status | applicant_ids: List.insert_at(status.applicant_ids, index, applicant_id)}

      # update the list of statuses
      statuses = state.statuses
      |> List.replace_at(status_index, status)

      # update the state
      state = state
      |> Map.put(:statuses, statuses)

      _ = Logger.debug(fn -> "Applicant #{applicant_id} added to status '#{status.name}'" end)

      {:reply, :ok, state}
    else
      _ ->
        _ = Logger.warn("One of the given IDs was not found")

        {:reply, :error, state}
    end
  end

  def handle_call({:remove_applicant_status, applicant_id}, _src, state) do
    # first find the status that contains this applicant (if any)
    found_tuple = state.statuses
    |> Enum.with_index()
    |> Enum.find_value(fn {status, status_index} ->
      with applicant_index when not is_nil(applicant_index) <- Enum.find_index(status.applicant_ids, fn id -> id == applicant_id end) do
        {status, status_index, applicant_index}
      else
        _ -> false
      end
    end)

    state = case found_tuple do
      nil ->
        _ = Logger.debug(fn -> "No status yet for applicant #{applicant_id}" end)
        state
      {status, status_index, applicant_index} ->
        # that applicant belongs to a status: remove him
        status = %{status | applicant_ids: List.delete_at(status.applicant_ids, applicant_index)}

        # update the list of statuses
        statuses = state.statuses
        |> List.replace_at(status_index, status)

        # update the state
        state = state
        |> Map.put(:statuses, statuses)

        _ = Logger.debug(fn -> "Applicant #{applicant_id} removed from status '#{status.name}'" end)

        state
    end

    {:reply, :ok, state}
  end

  def handle_call({:insert_status, %Status{} = status}, _src, state) do
    status = status
    |> set_unique_id_if_needed()

    # new entry is simply appended here
    state = state
    |> Map.put(:statuses, state.statuses ++ [status])

    _ = Logger.debug(fn -> "Status '#{status.name}' inserted with id #{status.id}" end)

    {:reply, status.id, state}
  end

  def handle_call({:insert_applicant, %Applicant{} = applicant}, _src, state) do
    applicant = applicant
    |> set_unique_id_if_needed()

    # new entry is simply appended here
    state = state
    |> Map.put(:applicants, state.applicants ++ [applicant])

    _ = Logger.debug(fn -> "Applicant '#{applicant.name}' inserted with id #{applicant.id}" end)

    {:reply, applicant.id, state}
  end

  # if no position set, return the end of the list
  defp check_position(nil, status_applicants_list_length), do: status_applicants_list_length
  defp check_position(position, status_applicants_list_length) do
    # make sure the given position has valid limits
    position
    |> max(0)
    |> min(status_applicants_list_length)
  end

  defp set_unique_id_if_needed(%Applicant{id: id} = applicant) when is_nil(id) do
    %{applicant | id: UniqueId.generate()}
  end
  defp set_unique_id_if_needed(%Applicant{} = applicant), do: applicant
  defp set_unique_id_if_needed(%Status{id: id} = status) when is_nil(id) do
    %{status | id: UniqueId.generate()}
  end
  defp set_unique_id_if_needed(%Status{} = status), do: status

  defp find_index_of_item_with_id(list, element_id) do
    list
    |> Enum.find_index(fn item ->
      item.id == element_id
    end)
  end

end
