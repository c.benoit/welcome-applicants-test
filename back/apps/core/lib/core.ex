defmodule Core do
  @moduledoc """
    Main business logic here
  """

  # Dependency injection: the implementation of a datastore can be swapped in the config file
  def data_store_module(), do: Application.get_env(:core, :data_store)

  @doc """
    Moves the applicant to another status to the end of the list of applicants for that status
    or at a given position (order)
  """
  def update_applicant_status(applicant_id, new_status_id, position \\ nil) do
    case data_store_module().remove_applicant_status(applicant_id) do
      :ok -> data_store_module().set_applicant_status(applicant_id, new_status_id, position)
      error -> error
    end
  end

  @doc """
    Lists all statuses
  """
  def get_statuses() do
    data_store_module().get_statuses()
  end

  @doc """
    Lists all applicants
  """
  def get_applicants() do
    data_store_module().get_applicants()
  end

end
