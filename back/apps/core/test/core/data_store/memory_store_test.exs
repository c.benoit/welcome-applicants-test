defmodule Core.DataStore.MemoryStoreTest do
  use ExUnit.Case
  alias Core.DataStore.MemoryStore
  alias Core.Model.{Applicant, Status}

  @applicant1   %Applicant{name: "Steve Jobs", title: "Apple Guru"}
  @applicant2   %Applicant{id: "3", name: "Dave Lauper", title: "Geek", avatar: "https://www.microsoft.com"}

  test "applicants insert + get" do
    MemoryStore.clear()

    applicant1_id = @applicant1
    |> MemoryStore.insert_applicant()
    |> check_unique_id()

    [applicant] = MemoryStore.get_applicants()

    assert applicant.id == applicant1_id
    assert applicant.name == @applicant1.name

    applicant2_id = @applicant2
    |> MemoryStore.insert_applicant()
    |> check_unique_id()

    assert applicant2_id != applicant1_id

    applicants = MemoryStore.get_applicants()
    assert Enum.count(applicants) == 2
  end

  @status1    %Status{name: "To contact"}
  @status2    %Status{id: "A34", name: "To hire"}

  test "status insert + get" do
    MemoryStore.clear()

    status1_id = @status1
    |> MemoryStore.insert_status()
    |> check_unique_id()

    [status] = MemoryStore.get_statuses()

    assert status.id == status1_id
    assert status.name == @status1.name

    MemoryStore.insert_status(@status2)
    |> check_unique_id()

    statuses = MemoryStore.get_statuses()
    assert Enum.count(statuses) == 2
  end

  test "clear" do
    MemoryStore.clear()

    assert MemoryStore.get_applicants() == []
    assert MemoryStore.get_statuses() == []

    MemoryStore.insert_applicant(@applicant1)
    MemoryStore.insert_status(@status1)

    assert Enum.count(MemoryStore.get_applicants()) == 1
    assert Enum.count(MemoryStore.get_statuses()) == 1

    MemoryStore.clear()

    assert MemoryStore.get_applicants() == []
    assert MemoryStore.get_statuses() == []
  end

  test "applicant status : set + remove" do
    MemoryStore.clear()

    applicant1_id = @applicant1
    |> MemoryStore.insert_applicant()

    applicant2_id = @applicant2
    |> MemoryStore.insert_applicant()

    status1_id = @status1
    |> MemoryStore.insert_status()

    assert :ok == MemoryStore.remove_applicant_status(applicant1_id)

    assert :ok == MemoryStore.set_applicant_status(applicant1_id, status1_id)
    [status1] = MemoryStore.get_statuses()
    assert status1.applicant_ids == [applicant1_id]

    assert :ok == MemoryStore.remove_applicant_status(applicant1_id)
    [status1] = MemoryStore.get_statuses()
    assert status1.applicant_ids == []

    status2_id = @status2
    |> MemoryStore.insert_status()

    assert :ok == MemoryStore.set_applicant_status(applicant1_id, status1_id)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status1_id).applicant_ids == [applicant1_id]
    assert find_status(statuses, status2_id).applicant_ids == []

    assert :ok == MemoryStore.remove_applicant_status(applicant1_id)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status1_id).applicant_ids == []

    assert :ok == MemoryStore.set_applicant_status(applicant1_id, status2_id)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status1_id).applicant_ids == []
    assert find_status(statuses, status2_id).applicant_ids == [applicant1_id]

    # test insertion position

    assert :ok == MemoryStore.set_applicant_status(applicant2_id, status2_id, 0)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status2_id).applicant_ids == [applicant2_id, applicant1_id]

    assert :ok == MemoryStore.remove_applicant_status(applicant2_id)

    assert :ok == MemoryStore.set_applicant_status(applicant2_id, status2_id, 1)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status2_id).applicant_ids == [applicant1_id, applicant2_id]

    assert :ok == MemoryStore.remove_applicant_status(applicant2_id)

    # negative position leads to position 0
    assert :ok == MemoryStore.set_applicant_status(applicant2_id, status2_id, -1)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status2_id).applicant_ids == [applicant2_id, applicant1_id]

    assert :ok == MemoryStore.remove_applicant_status(applicant2_id)

    # position greater than the list length means end of the list
    assert :ok == MemoryStore.set_applicant_status(applicant2_id, status2_id, 999)
    statuses = MemoryStore.get_statuses()
    assert find_status(statuses, status2_id).applicant_ids == [applicant1_id, applicant2_id]
  end

  @tag capture_log: true
  test "applicant status: error cases" do
    MemoryStore.clear()

    applicant1_id = @applicant1
    |> MemoryStore.insert_applicant()

    status1_id = @status1
    |> MemoryStore.insert_status()

    assert :error == MemoryStore.set_applicant_status(applicant1_id, "wrong")
    assert :error == MemoryStore.set_applicant_status("unknown", status1_id)
  end

  defp find_status(statuses, status_id) do
    statuses
    |> Enum.find(fn status -> status.id == status_id end)
  end

  defp check_unique_id(string) do
    assert string
    assert String.length(string) > 0

    string
  end
end
