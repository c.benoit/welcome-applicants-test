# Contains all mocks used within tests here
# https://hexdocs.pm/mox/Mox.html
Mox.defmock(MockStore, for: Core.DataStore)
