defmodule CoreTest do
  use ExUnit.Case, async: true
  import Mox

  setup :verify_on_exit!

  @applicant_id   "Elvis"
  @status_id      "Living"
  @position       3

  test "happy path" do
    previous_data_store = setup_mock_data_store()

    MockStore
    |> expect(:remove_applicant_status, 1, fn @applicant_id -> :ok end)

    MockStore
    |> expect(:set_applicant_status, 1, fn @applicant_id, @status_id, @position -> :ok end)

    Core.update_applicant_status(@applicant_id, @status_id, @position)

    MockStore
    |> expect(:get_statuses, 1, fn -> [] end)

    Core.get_statuses()

    MockStore
    |> expect(:get_applicants, 1, fn -> [] end)

    Core.get_applicants()

    restore_previous_data_store(previous_data_store)
  end

  test "happy path with no position" do
    previous_data_store = setup_mock_data_store()

    MockStore
    |> expect(:remove_applicant_status, 1, fn @applicant_id -> :ok end)

    MockStore
    |> expect(:set_applicant_status, 1, fn @applicant_id, @status_id, nil -> :ok end)

    Core.update_applicant_status(@applicant_id, @status_id)

    restore_previous_data_store(previous_data_store)
  end

  test "error path" do
    previous_data_store = setup_mock_data_store()

    MockStore
    |> expect(:remove_applicant_status, 1, fn @applicant_id -> :error end)

    MockStore
    |> expect(:set_applicant_status, 0, fn @applicant_id, @status_id, @position -> :ok end)

    Core.update_applicant_status(@applicant_id, @status_id, @position)

    restore_previous_data_store(previous_data_store)
  end

  # makes sure we set up our mock data store
  defp setup_mock_data_store() do
    previous_data_store = Application.get_env(:core, :data_store)
    Application.put_env(:core, :data_store, MockStore)

    previous_data_store
  end


  defp restore_previous_data_store(previous_data_store) do
    Application.put_env(:core, :data_store, previous_data_store)
  end
end
