defmodule ApiWeb.WelcomeChannel do
  use Phoenix.Channel
  require Logger

  def join("welcome:notifications", _message, socket) do
    _ = Logger.debug("Client joined room")
    {:ok, socket}
  end
  def join(room, _message, socket) do
    _ = Logger.warn(fn -> "Client tried to reach unknown room '#{room}'" end)
    {:error, socket}
  end

  # simple ping event to test data coming in
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end
end
