defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ApiWeb do
    pipe_through :api

    scope "/applicants" do
      get "/", ApplicantController, :index
      post "/:id", ApplicantController, :update
    end

    scope "/statuses" do
      get "/", StatusController, :index
    end
  end
end
