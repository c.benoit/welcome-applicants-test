defmodule ApiWeb.ApplicantController do
  @moduledoc """
  Applicants API resource controller
  """
  use ApiWeb, :controller
  alias ApiWeb.Endpoint
  require Logger

  def index(conn, _params) do
    conn
    |> json(%{
      "applicants" => Core.get_applicants()
      })
  end

  def update(conn, %{"id" => applicant_id, "status" => status_id} = params) do
    # optional position parameter
    position = Map.get(params, "position")

    _ = Core.update_applicant_status(applicant_id, status_id, position)

    _ = Logger.debug("Broadcasting update notification to channel")

    # broadcast notification to reload front data
    Endpoint.broadcast! "welcome:notifications", "update", %{"applicant" => applicant_id, "status" => status_id}

    conn
    |> json(%{})
  end
end
