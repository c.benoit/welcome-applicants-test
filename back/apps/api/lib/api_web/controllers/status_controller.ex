defmodule ApiWeb.StatusController do
  @moduledoc """
  Statuses API resource controller
  """
  use ApiWeb, :controller

  def index(conn, _params) do
    conn
    |> json(%{
      "statuses" => Core.get_statuses()
      })
  end
end
