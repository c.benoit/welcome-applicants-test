defmodule ApiWeb.ApplicantControllerTest do
  use ApiWeb.ConnCase
  alias Core.DataStore.MemoryStore

  test "GET /api/applicants - empty dataset", %{conn: conn} do
    MemoryStore.clear()

    conn = get(conn, "/api/applicants")
    assert Map.get(json_response(conn, 200), "applicants") == []
  end

  test "GET /api/applicants - 1 applicant", %{conn: conn} do
    Fixtures.one_applicant_two_statuses()

    conn = get(conn, "/api/applicants")
    assert Enum.count(Map.get(json_response(conn, 200), "applicants")) == 1
  end

  test "POST /api/applicants/:id", %{conn: conn} do
    Fixtures.one_applicant_two_statuses()

    [applicant] = Core.get_applicants()
    statuses = Core.get_statuses()

    free_status = statuses
    |> Enum.find(fn status -> Enum.count(status.applicant_ids) == 0 end)

    conn = post(conn, "/api/applicants/#{applicant.id}", %{"status" => free_status.id})
    assert json_response(conn, 200)

    new_status = Core.get_statuses()
    |> Enum.find(fn status -> status.id == free_status.id end)

    assert Enum.count(new_status.applicant_ids) == 1
  end
end
