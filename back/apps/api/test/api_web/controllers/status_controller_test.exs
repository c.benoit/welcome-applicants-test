defmodule ApiWeb.StatusControllerTest do
  use ApiWeb.ConnCase

  test "GET /api/statuses - empty dataset", %{conn: conn} do
    Core.DataStore.MemoryStore.clear()

    conn = get(conn, "/api/statuses")
    assert Map.get(json_response(conn, 200), "statuses") == []
  end

  test "GET /api/statuses - two statuses", %{conn: conn} do
    Fixtures.one_applicant_two_statuses()

    conn = get(conn, "/api/statuses")
    assert Enum.count(Map.get(json_response(conn, 200), "statuses")) == 2
  end
end
