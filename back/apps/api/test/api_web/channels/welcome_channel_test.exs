defmodule ApiWeb.WelcomeChannelTest do
  use ApiWeb.ChannelCase
  alias ApiWeb.{UserSocket, WelcomeChannel}

  test "connect and ping replies with status ok" do
    {:ok, _, socket} =
      UserSocket
      |> socket("user_id", %{})
      |> subscribe_and_join(WelcomeChannel, "welcome:notifications")

    payload = %{"hello" => "there"}
    ref = push(socket, "ping", payload)
    assert_reply ref, :ok, payload
  end

  @tag capture_log: true
  test "joining wrong channel" do
    {ret, _socket} =
      UserSocket
      |> socket("user_id", %{})
      |> subscribe_and_join(WelcomeChannel, "something:wrong")

    assert ret == :error
  end
end
